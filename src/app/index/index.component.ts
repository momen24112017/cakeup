import { Component, OnInit,ViewChild } from '@angular/core';
import { SlickCarouselComponent } from "ngx-slick-carousel";


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  @ViewChild('slickModal', { static: true }) slickModal: SlickCarouselComponent;
  slideConfig = {
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    autoplaySpeed: 3000,
    speed: 1500,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  next() {
    this.slickModal.slickNext();
  }
  prev() {
    this.slickModal.slickPrev();
  }

  categories = [{image: "assets/image/pro-1.png"},{image: "assets/image/pro-2.png"},{image: "assets/image/pro-3.png"},{image: "assets/image/pro-4.png"}, {image: "assets/image/pro-5.png"}];
 
  @ViewChild('slickModalCelb', { static: true }) slickModalCelb: SlickCarouselComponent;
  slideConfigCelb = {
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    autoplaySpeed: 3000,
    speed: 1500,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  nextCelb() {
    this.slickModalCelb.slickNext();
  }
  prevCelb() {
    this.slickModalCelb.slickPrev();
  }
  celebreties = [{image: "assets/image/celeb-1.png"},{image: "assets/image/celeb-2.png"},{image: "assets/image/celeb-3.png"},{image: "assets/image/celeb-4.png"}, {image: "assets/image/celeb-5.png"}];
 
  constructor() { }

  ngOnInit(): void {
  }

}
